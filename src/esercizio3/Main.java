package esercizio3;
import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		print("Menu: -1 per es1  -2 per es2 -3 per es3 ");
		String opz=in();
		int opzi=toInt(opz);
		switch(opzi){
			case 1:
				es1();
				break;
			case 2:
				es2();
				break;
			case 3:
				es3();
				break;
			case 4:
				es4();
				break;
			default:
				print("nessuna scelta riconosciuta");
		}
	}
	
	static void print(String stinga) {
		System.out.println(stinga);
	}
	static String in() {
		Scanner scan=new Scanner(System.in);
		return scan.nextLine();
	}
	static void es1() {
		Random rnd = new Random();
		int[] arr = new int[5];
		for(int i=0;i<5;i++) {
			arr[i]=rnd.nextInt(10);
		}
		print(Arrays.toString(arr));
		Arrays.sort(arr);
		print(Arrays.toString(arr));
	}
	static void es2() {
		print("inserisci una parola");
		String str = in();
		int len=0;
		boolean factor =false,check=false;
		if(str.length()%2==0)len=str.length()/2;
		else {
			len=((str.length()-1)/2)+1;
			factor=true;
		}
		print(Integer.toString(len));
		for(int i=0;i<=len;i++) {
			if(str.charAt(i)!=str.charAt(str.length()-(i+1))) {
				print("la stringa non � palindroma");
				break;
			}
			if((i==len && factor==false)||(i==(len-1)&& factor==true))print("la stringa � palindroma");
		}
	}
	static int toInt(String str) {
		int i=0;
		try {
			i=Integer.parseInt(str);
		}catch(Exception ex) {
			print("c'� stato un errore nel casting di "+str);
		}
		return i;
	}
	static String toString(int[] arr ) {
		String str="";
		for(int i=0;i<arr.length;i++) {
			str=str+arr[i];
			str+=",";
		}
		return str;
	}
	static void es3() {
		print("fino a che punto della serie vuoi arrivare");
		int x=toInt(in());
		double[] serie={0,1,0};
		print(Integer.toString(1));
		for(int i=0;i<x;i++) {
			serie[2]=serie[0]+serie[1];
			print(Double.toString(serie[2]));
			serie[0]=serie[1];
			serie[1]=serie[2];
		}
	}
	static void es4() {
		int[]riga1= {1,2,3};
		int[]riga2= {4,5,6};
		int[]riga3= {6,7,8};
		int[][]matr= {Arrays.copyOf(riga1, riga1.length),Arrays.copyOf(riga2, riga1.length),Arrays.copyOf(riga3, riga1.length)};
		//print(Arrays.toString(matr[0]));
		int[][]matrI=new int[matr[0].length][matr.length];
		for(int i=0;i<matr[0].length;i++) {
			for(int j=0;j<matr.length;j++) {
				matrI[i][j]=matr[j][i];
			}
			print(Arrays.toString(matrI[i]));
		}
		for (int[] c : matr) {
			print(Arrays.toString(c));
		}
		
		
	}
	
}
